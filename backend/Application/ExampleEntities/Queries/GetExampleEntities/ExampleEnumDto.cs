﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.ExampleEntities.Queries.GetExampleEntities
{
    public class ExampleEnumDto
    {
        public int Value { get; set; }

        public string Name { get; set; }
    }
}
