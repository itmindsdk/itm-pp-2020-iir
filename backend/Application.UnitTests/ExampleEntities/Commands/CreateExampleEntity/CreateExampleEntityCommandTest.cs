﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.ExampleEntities.Commands.CreateExampleEntity;
using Domain.Enums;
using Shouldly;
using Xunit;

namespace Application.UnitTests.ExampleEntities.Commands.CreateExampleEntity
{
    public class CreateExampleEntityCommandTest : CommandTestBase
    {
        [Fact]
        public async Task Handle_ShouldPersistExampleEntity()
        {
            var command = new CreateExampleEntityCommand
            {
                Name = "CreateTest",
                ExampleEnum = ExampleEnum.A
            };

            var handler = new CreateExampleEntityCommand.CreateExampleEntityCommandHandler(Context);

            var result = await handler.Handle(command, CancellationToken.None);

            var entity = Context.ExampleEntities.Find(result);

            entity.ShouldNotBeNull();
            entity.Name.ShouldBe(command.Name);
            entity.ExampleEnum.ShouldBe(command.ExampleEnum);
        }
    }
}
